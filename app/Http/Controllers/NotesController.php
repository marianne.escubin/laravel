<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotesController extends Controller
{
    function showTopics() {
	    $topics = ["routes", "views", "controllers"]; 
		return view('notes/learnings', compact('topics'));
    }
}
